package cr.ac.ucenfotec.ui;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class UI {

    private PrintStream out = System.out;
    private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public void mostrarMenu(){
        out.println("1.Registrar Propiedad.");
        out.println("2.Modificar Propiedad.");
        out.println("3.Eliminar Propiedad.");
        out.println("4.Buscar Propiedad.");
        out.println("5.Listar Propiedades.");
        out.println("6.Alquilar Propiedad.");
        out.println("7.Obtener monto total de alquiler.");
        out.println("8.Obtener cantidad de Propiedades.");
        out.println("9.Registrar inquilino.");
        out.println("10.Modificar inquilino.");
        out.println("11.Eliminar inquilino.");
        out.println("12.Buscar inquilino.");
        out.println("13.Listar inquilinos.");
        out.println("14.Obtener cantidad de inquilinos.");
        out.println("0.Salir");
        out.println("Digite la opción que desee: ");
    }

    public int leerOpcion() throws Exception {
        return Integer.parseInt(in.readLine());
    }

    public void imprimirMensaje(String mensaje){
        out.println(mensaje);
    }

    public String leerTexto() throws Exception {
        return in.readLine();
    }

}
