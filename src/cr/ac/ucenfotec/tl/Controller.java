package cr.ac.ucenfotec.tl;

import cr.ac.ucenfotec.bl.logic.PropiedadGestor;
import cr.ac.ucenfotec.ui.UI;

public class Controller {

    private UI interfaz;
    private PropiedadGestor gestorProp;

    public Controller(){
        interfaz = new UI();
        gestorProp = new PropiedadGestor();
    }

    public void start() throws Exception {
        int opcion = -1;
        do{
            interfaz.mostrarMenu();
            opcion = interfaz.leerOpcion();
            procesarOpcion(opcion);
        }while(opcion != 0);
    }

    //Rutina para procesar la opción seleccionada por el usuario
    public void procesarOpcion(int pOpcion) throws Exception{
        switch(pOpcion){
            case 1:
                registrarPropiedad();
                break;
            case 2:
                //
                break;
            case 3:
                //
                break;
            case 4:
                //
                break;
            case 5:
                listarPropiedades();
                break;
            case 6:
                alquilarPropiedad();
                break;
            case 7:
                //
                break;
            case 8:
                //
                break;
            case 9:
                registrarInquilino();
                break;
            case 10:
                //
                break;
            case 11:
                //
                break;
            case 12:
                buscarInquilino();
                break;
            case 13:
                listarInquilinos();
                break;
            case 14:
                obtenerCantidadInquilinos();
                break;
            case 0:
                interfaz.imprimirMensaje("Adiós");
                break;
            default:
                interfaz.imprimirMensaje("Opción inválida");
        }
    }

    public void registrarPropiedad() throws Exception {
        interfaz.imprimirMensaje("Digite el codigo: ");
        int codigo = Integer.parseInt(interfaz.leerTexto());
        interfaz.imprimirMensaje("Digite el nombre: ");
        String nombre = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite la cantidad de cuartos: ");
        int cuartos = Integer.parseInt(interfaz.leerTexto());
        interfaz.imprimirMensaje("Digite provincia: ");
        String provincia = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite el costo: ");
        double costo = Double.parseDouble(interfaz.leerTexto());

        String resultado = gestorProp.registrarPropiedad(codigo,nombre,cuartos,provincia,costo,false);
        interfaz.imprimirMensaje(resultado);
    }

    public void listarPropiedades() throws Exception {
        gestorProp.listarPropiedades().forEach(propiedad -> interfaz.imprimirMensaje(propiedad.toString()));
    }

    public void registrarInquilino() throws Exception {
        interfaz.imprimirMensaje("Digite la cedula:");
        int cedula = Integer.parseInt(interfaz.leerTexto());
        interfaz.imprimirMensaje("Digite el nombre:");
        String nombre = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite la profesión:");
        String profesion = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite la edad:");
        int edad = Integer.parseInt(interfaz.leerTexto());

    }

    public void buscarInquilino() throws Exception {
        interfaz.imprimirMensaje("Digite la cedula:");
        int cedula = Integer.parseInt(interfaz.leerTexto());

    }
    public void alquilarPropiedad() throws Exception{
        interfaz.imprimirMensaje("Digite la cedula:");
        int cedula = Integer.parseInt(interfaz.leerTexto());

        listarPropiedades();
        interfaz.imprimirMensaje("Digite el codigo de la propiedad: ");
        int codigo = Integer.parseInt(interfaz.leerTexto());

        String mensaje =gestorProp.alquilarPropiedad(cedula,codigo);
        interfaz.imprimirMensaje(mensaje);
    }

    public void obtenerCantidadInquilinos() throws Exception {
    }

    public void listarInquilinos() throws Exception{

    }

}
